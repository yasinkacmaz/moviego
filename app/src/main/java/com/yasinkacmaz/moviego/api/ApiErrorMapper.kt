package com.yasinkacmaz.moviego.api

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.yasinkacmaz.moviego.api.exception.ApiException
import com.yasinkacmaz.moviego.api.exception.ApiException.KnownApiException
import com.yasinkacmaz.moviego.api.exception.ApiException.UnknownApiException
import com.yasinkacmaz.moviego.api.response.ErrorResponse
import com.yasinkacmaz.moviego.utils.Mapper
import retrofit2.HttpException
import javax.inject.Inject

class ApiErrorMapper @Inject constructor(private val gson: Gson) : Mapper<Throwable, ApiException> {

    override fun map(input: Throwable): ApiException {
        val errorBody = (input as? HttpException)?.response()?.errorBody()?.string()
        errorBody ?: return UnknownApiException()

        return try {
            val errorResponse: ErrorResponse? = gson.fromJson(errorBody, ErrorResponse::class.java)
            if (errorResponse == null) {
                UnknownApiException()
            } else {
                KnownApiException(errorResponse)
            }
        } catch (exception: JsonSyntaxException) {
            UnknownApiException()
        }
    }
}
