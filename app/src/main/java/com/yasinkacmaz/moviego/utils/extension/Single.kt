package com.yasinkacmaz.moviego.utils.extension

import com.yasinkacmaz.moviego.api.ApiErrorMapper
import com.yasinkacmaz.moviego.api.response.ApiResponse
import com.yasinkacmaz.moviego.ui.viewmodel.BaseViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T : ApiResponse> Single<T>.checkApiError(apiErrorMapper: ApiErrorMapper): Single<T> {
    return onErrorResumeNext { Single.error(apiErrorMapper.map(it)) }
}

fun <T> Single<T>.applySchedulers(): Single<T> {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.progressify(viewModel: BaseViewModel): Single<T> {
    return this.doOnSubscribe { viewModel.onProgress.value = true }
        .doOnEvent { _, _ -> viewModel.onProgress.value = false }
}
