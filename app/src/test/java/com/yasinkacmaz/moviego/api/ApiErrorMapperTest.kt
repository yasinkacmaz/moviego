package com.yasinkacmaz.moviego.api

import com.google.gson.Gson
import com.yasinkacmaz.moviego.api.exception.ApiException.KnownApiException
import com.yasinkacmaz.moviego.api.exception.ApiException.UnknownApiException
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response

class ApiErrorMapperTest {

    private val gson = Gson()
    private val mapper = ApiErrorMapper(gson)
    private val jsonMediaType = MediaType.parse("application/json")

    @Test
    fun `map when input is not a HttpException should return UnknownApiException`() {
        assertThat(mapper.map(Throwable())).isInstanceOf(UnknownApiException::class.java)
    }

    @Test
    fun `map when input is HttpException but gson parse fail should return UnknownApiException`() {
        val response: Response<Any> = Response.error(400, ResponseBody.create(jsonMediaType, ""))
        val httpException = HttpException(response)

        assertThat(mapper.map(httpException)).isInstanceOf(UnknownApiException::class.java)
    }

    @Test
    fun `map when input is HttpException and gson can parse should return KnownApiException`() {
        val response: Response<Any> = Response.error(
            400,
            ResponseBody.create(jsonMediaType, "{\"status_code\": 1,\"status_message\": \"Error\"}")
        )

        val exception = mapper.map(HttpException(response))

        assertThat(exception).isInstanceOf(KnownApiException::class.java)
        with(exception as KnownApiException) {
            assertThat(errorResponse.statusCode).isEqualTo(1)
            assertThat(errorResponse.statusMessage).isEqualTo("Error")
        }
    }
}
