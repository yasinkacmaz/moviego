package com.yasinkacmaz.moviego.application

import android.app.Application
import com.yasinkacmaz.moviego.di.ActivityModule
import com.yasinkacmaz.moviego.di.ModelModule
import com.yasinkacmaz.moviego.di.NetworkModule
import com.yasinkacmaz.moviego.di.UtilityModule
import com.yasinkacmaz.moviego.ui.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ApplicationModule::class, AndroidInjectionModule::class, ActivityModule::class,
        ViewModelModule::class, NetworkModule::class, ModelModule::class, UtilityModule::class]
)
interface ApplicationComponent {

    fun inject(movieGoApplication: MovieGoApplication)

    @Component.Builder
    interface Builder {

        fun build(): ApplicationComponent

        @BindsInstance
        fun application(application: Application): Builder
    }
}
