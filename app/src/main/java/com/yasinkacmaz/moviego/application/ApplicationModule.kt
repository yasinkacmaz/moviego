package com.yasinkacmaz.moviego.application

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Suppress("UnnecessaryAbstractClass")
@Module
abstract class ApplicationModule {
    @Binds
    @Singleton
    abstract fun provideApplicationContext(application: Application): Context
}
