package com.yasinkacmaz.moviego.di

import com.yasinkacmaz.moviego.api.MovieService
import com.yasinkacmaz.moviego.model.MoviesModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModelModule {
    @Singleton
    @Provides
    fun provideMoviesModel(movieService: MovieService): MoviesModel = MoviesModel(movieService)
}
