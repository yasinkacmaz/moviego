package com.yasinkacmaz.moviego.ui

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.yasinkacmaz.moviego.R
import java.util.UUID

class AlertDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val message = arguments!!.getString(ARG_MESSAGE)

        return AlertDialog.Builder(activity)
            .setTitle(getString(R.string.app_name))
            .setMessage(message)
            .setPositiveButton(R.string.ok) { _, _ ->
                dismiss()
            }
            .create()
    }

    companion object {
        private const val ARG_MESSAGE = "message"

        fun show(fragmentManager: FragmentManager, message: String) {
            AlertDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_MESSAGE, message)
                }
                show(fragmentManager, UUID.randomUUID().toString())
            }
        }
    }
}
