package com.yasinkacmaz.moviego.utils

interface Mapper<in Input, out Output> {
    fun map(input: Input): Output
}
