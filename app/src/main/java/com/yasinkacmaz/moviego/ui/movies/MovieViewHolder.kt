package com.yasinkacmaz.moviego.ui.movies

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.squareup.picasso.Picasso
import com.yasinkacmaz.moviego.utils.extension.loadSafely
import com.yasinkacmaz.moviego.utils.extension.toImageUrl
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_movie.descriptionTextView
import kotlinx.android.synthetic.main.item_movie.nameTextView
import kotlinx.android.synthetic.main.item_movie.posterImageView
import kotlinx.android.synthetic.main.item_movie.rateTextView
import kotlinx.android.synthetic.main.item_movie.voteCountTextView

class MovieViewHolder(override val containerView: View) : ViewHolder(containerView),
    LayoutContainer {

    fun bindTo(movieUiModel: MovieUiModel, picasso: Picasso) {
        picasso.loadSafely(posterImageView, movieUiModel.posterPath.toImageUrl())
        nameTextView.text = movieUiModel.name
        descriptionTextView.text = movieUiModel.description
        rateTextView.text = movieUiModel.rate.toString()
        voteCountTextView.text = movieUiModel.voteCount.toString()
    }
}
