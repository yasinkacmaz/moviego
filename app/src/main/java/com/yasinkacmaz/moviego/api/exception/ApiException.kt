package com.yasinkacmaz.moviego.api.exception

import com.yasinkacmaz.moviego.api.response.ErrorResponse

sealed class ApiException : Throwable() {
    data class KnownApiException(val errorResponse: ErrorResponse) : ApiException()
    class UnknownApiException : ApiException()
}
