package com.yasinkacmaz.moviego.application

import kotlin.annotation.AnnotationRetention.RUNTIME

@Retention(RUNTIME)
@MustBeDocumented
annotation class ActivityScope
