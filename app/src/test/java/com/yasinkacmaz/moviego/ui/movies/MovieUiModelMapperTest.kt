package com.yasinkacmaz.moviego.ui.movies

import com.yasinkacmaz.moviego.api.response.Movie
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MovieUiModelMapperTest {
    @Test
    fun map() {
        val mapper = MovieUiModelMapper()

        val name = "movieName"
        val language = "language"
        val date = "01.03.1337"
        val posterUrl = "posterUrl"
        val description = "description"
        val votes = 1337
        val averageVote = 13.37

        val movie = Movie(date, name, language, description, posterUrl, averageVote, votes)

        val expectedMovieUiModel = MovieUiModel(
            name = name,
            language = language,
            date = date,
            posterPath = posterUrl,
            description = description,
            voteCount = votes,
            rate = averageVote
        )

        assertThat(mapper.map(listOf(movie)).first()).isEqualTo(expectedMovieUiModel)
    }
}
