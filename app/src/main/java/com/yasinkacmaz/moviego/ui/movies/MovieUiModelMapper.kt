package com.yasinkacmaz.moviego.ui.movies

import com.yasinkacmaz.moviego.api.response.Movie
import com.yasinkacmaz.moviego.utils.Mapper
import javax.inject.Inject

class MovieUiModelMapper @Inject constructor() : Mapper<List<Movie>, List<MovieUiModel>> {
    override fun map(input: List<Movie>): List<MovieUiModel> {
        return input.map { movie ->
            MovieUiModel(
                name = movie.name,
                language = movie.originalLanguage,
                date = movie.firstAirDate,
                posterPath = movie.posterPath.orEmpty(),
                description = movie.overview,
                voteCount = movie.voteCount,
                rate = movie.voteAverage
            )
        }
    }
}
