package com.yasinkacmaz.moviego.utils.extension

import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.yasinkacmaz.moviego.R

fun Picasso.loadSafely(target: ImageView, url: String) {
    load(url)
        .error(R.drawable.ic_broken_image_gray_24dp)
        .into(target)
}
