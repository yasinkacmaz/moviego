package com.yasinkacmaz.moviego.ui.movies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.yasinkacmaz.moviego.api.ApiErrorMapper
import com.yasinkacmaz.moviego.api.response.Movie
import com.yasinkacmaz.moviego.api.response.MoviesResponse
import com.yasinkacmaz.moviego.model.MoviesModel
import com.yasinkacmaz.moviego.utils.TestUtils
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MoviesViewModelTest {
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var movieModel: MoviesModel
    @Mock
    private lateinit var apiErrorMapper: ApiErrorMapper
    @Mock
    private lateinit var progressObserver: Observer<Boolean>
    @Mock
    private lateinit var errorObserver: Observer<Throwable>

    private val movieUiModelMapper = MovieUiModelMapper()

    private lateinit var moviesViewModel: MoviesViewModel

    private val totalMovieCount = 1337
    private val movie = Movie("", "", "", "", "", 1.3, 37)
    private val movies = listOf(movie)
    private val moviesResponse = MoviesResponse(1, movies, 13, totalMovieCount)

    @Before
    fun setUp() {
        TestUtils.useSingleThread()
        moviesViewModel = MoviesViewModel(movieModel, movieUiModelMapper, apiErrorMapper)
        moviesViewModel.onProgress.observeForever(progressObserver)
        moviesViewModel.onException.observeForever(errorObserver)
    }

    @Test
    fun `fetch movies success`() {
        whenever(movieModel.fetchMovies(any())).thenReturn(Single.just(moviesResponse))

        moviesViewModel.fetchMovies()

        verify(progressObserver).onChanged(true)
        verify(progressObserver).onChanged(false)
        assertThat(moviesViewModel.totalMovieCount.value).isEqualTo(totalMovieCount)
        assertThat(moviesViewModel.movieUiModels.value!!.first()).isEqualTo(movieUiModelMapper.map(movies).first())
    }

    @Test
    fun `fetch movies if not last page should increment page number`() {
        val currentPage = 3
        moviesViewModel.pageNumber = currentPage
        whenever(movieModel.fetchMovies(any())).thenReturn(Single.just(moviesResponse))

        moviesViewModel.fetchMovies()

        assertThat(moviesViewModel.pageNumber).isEqualTo(currentPage + 1)
    }

    @Test
    fun `fetch movies when last page reached should call lastPageReached`() {
        val currentPage = 13
        val lastPageObserver: Observer<Unit> = mock()
        moviesViewModel.lastPageReached.observeForever(lastPageObserver)
        moviesViewModel.pageNumber = currentPage
        whenever(movieModel.fetchMovies(any())).thenReturn(
            Single.just(
                moviesResponse.copy(
                    page = currentPage,
                    totalPages = currentPage
                )
            )
        )

        moviesViewModel.fetchMovies()

        verify(lastPageObserver).onChanged(any())
        assertThat(moviesViewModel.isLastPage).isEqualTo(true)
    }

    @Test
    fun `fetch movies error`() {
        val throwable = Throwable()

        whenever(movieModel.fetchMovies(any())).thenReturn(Single.error(throwable))

        moviesViewModel.fetchMovies()

        verify(progressObserver).onChanged(true)
        verify(progressObserver).onChanged(false)
        verify(apiErrorMapper).map(throwable)
        verify(errorObserver).onChanged(any())
    }
}
