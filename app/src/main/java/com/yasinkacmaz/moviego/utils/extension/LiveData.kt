package com.yasinkacmaz.moviego.utils.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

inline fun <T> LiveData<T>.observeWith(
    lifecycleOwner: LifecycleOwner,
    crossinline onChanged: (T) -> Unit
) {
    observe(lifecycleOwner, Observer {
        it ?: return@Observer
        onChanged.invoke(it)
    })
}

fun <T> MutableLiveData<MutableList<T>>.append(items: List<T>) {
    val currentItems = value!!
    currentItems.addAll(items)
    value = currentItems
}
