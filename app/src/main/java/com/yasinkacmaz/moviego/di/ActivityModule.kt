package com.yasinkacmaz.moviego.di

import com.yasinkacmaz.moviego.application.ActivityScope
import com.yasinkacmaz.moviego.ui.movies.MoviesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("UnnecessaryAbstractClass")
@Module
abstract class ActivityModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMoviesActivityInjector(): MoviesActivity
}
