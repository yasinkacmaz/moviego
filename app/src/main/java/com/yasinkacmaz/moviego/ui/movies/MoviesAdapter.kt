package com.yasinkacmaz.moviego.ui.movies

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.squareup.picasso.Picasso
import com.yasinkacmaz.moviego.R
import com.yasinkacmaz.moviego.utils.extension.inflate
import javax.inject.Inject

class MoviesAdapter @Inject constructor(
    private val picasso: Picasso
) : RecyclerView.Adapter<ViewHolder>() {

    private val movies = mutableListOf<AdapterItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            VIEW_TYPE_MOVIE -> MovieViewHolder(parent.inflate(R.layout.item_movie))
            VIEW_TYPE_LOADING -> LoadingViewHolder(parent.inflate(R.layout.item_loading))
            else -> throw IllegalStateException("Unsupported view type: $viewType")
        }
    }

    override fun getItemCount(): Int = movies.size

    override fun getItemViewType(position: Int): Int {
        return when (movies[position]) {
            is MovieUiModel -> VIEW_TYPE_MOVIE
            is LoadingItem -> VIEW_TYPE_LOADING
            else -> throw IllegalStateException("Unsupported view type for position: $position")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is MovieViewHolder) {
            holder.bindTo(movies[position] as MovieUiModel, picasso)
        }
    }

    fun showLoading(recyclerView: RecyclerView) {
        recyclerView.post {
            movies.add(LoadingItem)
            notifyItemInserted(movies.size)
        }
    }

    fun hideLoading(recyclerView: RecyclerView) {
        recyclerView.post {
            val loadMoreItemIndex = movies.indexOf(LoadingItem)
            movies.remove(LoadingItem)
            notifyItemRemoved(loadMoreItemIndex)
        }
    }

    fun showMovies(recyclerView: RecyclerView, movieUiModels: List<MovieUiModel>) {
        recyclerView.post {
            movies.addAll(movieUiModels)
            notifyItemRangeInserted(movies.size, movieUiModels.size)
        }
    }

    companion object {
        private const val VIEW_TYPE_MOVIE = 1
        const val VIEW_TYPE_LOADING = 2
    }
}
