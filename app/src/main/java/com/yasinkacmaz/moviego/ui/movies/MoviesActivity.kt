package com.yasinkacmaz.moviego.ui.movies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.yasinkacmaz.moviego.R
import com.yasinkacmaz.moviego.api.exception.ApiException.KnownApiException
import com.yasinkacmaz.moviego.ui.AlertDialogFragment
import com.yasinkacmaz.moviego.utils.PagingScrollListener
import com.yasinkacmaz.moviego.utils.extension.get
import com.yasinkacmaz.moviego.utils.extension.observeWith
import com.yasinkacmaz.moviego.utils.extension.unsafeLazy
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movies.moviesRecyclerView
import kotlinx.android.synthetic.main.activity_movies.toolbar
import javax.inject.Inject
import com.yasinkacmaz.moviego.ui.movies.MoviesAdapter.Companion.VIEW_TYPE_LOADING

class MoviesActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var moviesAdapter: MoviesAdapter

    private val moviesViewModel by unsafeLazy { viewModelFactory.get<MoviesViewModel>(this) }

    private lateinit var pagingListener: PagingScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        initToolbar()
        initRecyclerView()
        observeViewModel()
        moviesViewModel.fetchMovies()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.title_movies)
    }

    private fun initRecyclerView() {
        val gridLayoutManager = GridLayoutManager(this, SPAN_COUNT).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (moviesAdapter.getItemViewType(position)) {
                        VIEW_TYPE_LOADING -> return SPAN_COUNT
                        else -> 1
                    }
                }
            }
        }

        pagingListener = object : PagingScrollListener(gridLayoutManager) {
            override val isLastPage: Boolean = moviesViewModel.isLastPage

            override fun loadMoreItems() {
                moviesViewModel.fetchMovies()
            }
        }
        moviesRecyclerView.apply {
            setHasFixedSize(true)
            adapter = moviesAdapter
            layoutManager = gridLayoutManager
            addOnScrollListener(pagingListener)
        }
    }

    private fun observeViewModel() = with(moviesViewModel) {
        val moviesActivity = this@MoviesActivity

        onProgress.observeWith(moviesActivity) {
            if (it) {
                moviesAdapter.showLoading(moviesRecyclerView)
            } else {
                moviesAdapter.hideLoading(moviesRecyclerView)
            }
        }
        lastPageReached.observeWith(moviesActivity) {
            moviesRecyclerView.removeOnScrollListener(pagingListener)
        }

        totalMovieCount.observeWith(moviesActivity, ::showTotalMovieCount)
        movieUiModels.observeWith(moviesActivity) {
            moviesAdapter.showMovies(moviesRecyclerView, it)
        }

        onException.observeWith(moviesActivity, moviesActivity::onException)
    }

    private fun showTotalMovieCount(count: Int) {
        toolbar.subtitle = getString(R.string.count_movies_found, count)
    }

    private fun onException(throwable: Throwable) {
        val message = (throwable as? KnownApiException)?.localizedMessage
            ?: getString(R.string.an_error_occurred)

        AlertDialogFragment.show(supportFragmentManager, message)
    }

    companion object {
        private const val SPAN_COUNT = 2
    }
}
