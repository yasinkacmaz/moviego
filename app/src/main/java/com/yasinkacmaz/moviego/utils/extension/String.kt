package com.yasinkacmaz.moviego.utils.extension

private const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p"
private const val IMAGE_SIZE = "/w154"

fun String.toImageUrl(): String = "$IMAGE_BASE_URL$IMAGE_SIZE$this"
