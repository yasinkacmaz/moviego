package com.yasinkacmaz.moviego.ui.movies

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yasinkacmaz.moviego.api.ApiErrorMapper
import com.yasinkacmaz.moviego.model.MoviesModel
import com.yasinkacmaz.moviego.ui.viewmodel.BaseViewModel
import com.yasinkacmaz.moviego.utils.extension.append
import com.yasinkacmaz.moviego.utils.extension.applySchedulers
import com.yasinkacmaz.moviego.utils.extension.checkApiError
import com.yasinkacmaz.moviego.utils.extension.progressify
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class MoviesViewModel @Inject constructor(
    private val movieModel: MoviesModel,
    private val movieUiModelMapper: MovieUiModelMapper,
    private val apiErrorMapper: ApiErrorMapper
) : BaseViewModel() {

    @VisibleForTesting
    var pageNumber = FIRST_PAGE_NUMBER

    private val _movieUiModels = MutableLiveData<MutableList<MovieUiModel>>().apply {
        value = mutableListOf()
    }
    val movieUiModels: LiveData<MutableList<MovieUiModel>> = _movieUiModels

    private val _totalMovieCount = MutableLiveData<Int>()
    val totalMovieCount: LiveData<Int> = _totalMovieCount

    private val _lastPageReached = MutableLiveData<Unit>()
    val lastPageReached: LiveData<Unit> = _lastPageReached

    val isLastPage get() = lastPageReached.value != null

    fun fetchMovies() {
        movieModel.fetchMovies(pageNumber)
            .applySchedulers()
            .progressify(this)
            .checkApiError(apiErrorMapper)
            .doOnSuccess { checkNextPage(it.page, it.totalPages) }
            .doOnSuccess { _totalMovieCount.value = it.totalResults }
            .map { movieUiModelMapper.map(it.movies) }
            .subscribe({
                _movieUiModels.append(it)
            }, onException::setValue)
            .addTo(compositeDisposable)
    }

    private fun checkNextPage(currentPage: Int, totalPages: Int) {
        if (currentPage >= totalPages) {
            _lastPageReached.value = Unit
        } else {
            pageNumber++
        }
    }

    companion object {
        private const val FIRST_PAGE_NUMBER = 1
    }
}
