package com.yasinkacmaz.moviego.model

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.yasinkacmaz.moviego.api.MovieService
import com.yasinkacmaz.moviego.api.response.MoviesResponse
import com.yasinkacmaz.moviego.utils.TestUtils
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MoviesModelTest {

    @Mock
    private lateinit var movieService: MovieService

    private lateinit var moviesModel: MoviesModel
    private val moviesResponse = MoviesResponse(1, listOf(), 1, 1)

    @Before
    fun setUp() {
        TestUtils.useSingleThread()
        moviesModel = MoviesModel(movieService)
    }

    @Test
    fun fetchMovies() {
        val pageNumber = 1
        whenever(movieService.fetchMovies(pageNumber)).thenReturn(Single.just(moviesResponse))

        moviesModel
            .fetchMovies(pageNumber)
            .test()
            .assertValue(moviesResponse)

        verify(movieService).fetchMovies(pageNumber)
    }
}
