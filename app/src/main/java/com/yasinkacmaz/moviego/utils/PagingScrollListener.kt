package com.yasinkacmaz.moviego.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PagingScrollListener(
    private val layoutManager: LinearLayoutManager
) : RecyclerView.OnScrollListener() {

    private var previousTotal = 0
    private var loading = true
    private var lastVisibleItem = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0) {
            onScrolled()
        }
    }

    @Synchronized
    private fun onScrolled() {
        visibleItemCount = layoutManager.childCount
        totalItemCount = layoutManager.itemCount
        lastVisibleItem = layoutManager.findLastCompletelyVisibleItemPosition()

        if (totalItemCount < previousTotal) {
            previousTotal = totalItemCount
            if (totalItemCount == 0) {
                loading = true
            }
        }

        if (loading && totalItemCount > previousTotal + COUNT_FOR_LOADING_ITEM) {
            loading = false
            previousTotal = totalItemCount
        }

        if (!isLastPage && !loading && totalItemCount <= lastVisibleItem + LOAD_MORE_THRESHOLD) {
            loading = true
            loadMoreItems()
        }
    }

    abstract val isLastPage: Boolean
    abstract fun loadMoreItems()

    companion object {
        private const val COUNT_FOR_LOADING_ITEM = 1
        private const val LOAD_MORE_THRESHOLD = 2
    }
}
