package com.yasinkacmaz.moviego.model

import com.yasinkacmaz.moviego.api.MovieService
import com.yasinkacmaz.moviego.api.response.MoviesResponse
import io.reactivex.Single

class MoviesModel(private val movieService: MovieService) {
    fun fetchMovies(pageNumber: Int): Single<MoviesResponse> {
        return movieService.fetchMovies(pageNumber)
    }
}
