package com.yasinkacmaz.moviego.ui.movies

data class MovieUiModel(
    val name: String,
    val language: String,
    val date: String,
    val posterPath: String,
    val description: String,
    val voteCount: Int,
    val rate: Double
) : AdapterItem
