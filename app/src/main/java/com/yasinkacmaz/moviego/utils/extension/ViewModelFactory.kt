package com.yasinkacmaz.moviego.utils.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

inline fun <reified T : ViewModel> ViewModelProvider.Factory.get(activity: AppCompatActivity): T {
    return ViewModelProviders.of(activity, this)[T::class.java]
}
