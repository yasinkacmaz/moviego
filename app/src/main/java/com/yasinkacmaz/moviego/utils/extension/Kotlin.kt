package com.yasinkacmaz.moviego.utils.extension

import kotlin.LazyThreadSafetyMode.NONE

fun <T : Any> unsafeLazy(initializer: () -> T) = lazy(NONE, initializer)
