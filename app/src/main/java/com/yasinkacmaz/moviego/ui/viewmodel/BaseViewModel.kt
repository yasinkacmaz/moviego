package com.yasinkacmaz.moviego.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    val compositeDisposable = CompositeDisposable()

    val onProgress = MutableLiveData<Boolean>()

    val onException = MutableLiveData<Throwable>()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
