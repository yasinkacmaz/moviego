package com.yasinkacmaz.moviego.api

import com.yasinkacmaz.moviego.api.response.MoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {
    @GET("tv/popular")
    fun fetchMovies(@Query("page") pageNumber: Int): Single<MoviesResponse>
}
